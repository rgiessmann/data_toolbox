# Spectral unmixing data toolbox

A toolbox for analyzing enzymatic reaction data and perform spectral unmixing of UV/VIS spectra.

Background information on the assay which uses this toolbox can be found in the following open access publication:

 Kaspar, F.; Giessmann, R.T.; Krausch, N.; Neubauer, P.; Wagner, A.; Gimpel, M. A UV/Vis Spectroscopy-Based Assay for Monitoring of Transformations Between Nucleosides and Nucleobases. _Methods Protoc_. **2019**, _2_, 60. https://doi.org/10.3390/mps2030060

## Getting Started

These instructions will help you installing the data-toolbox on your system and guide you through the usage.

### Installing

First you need a proper python installation on your system. We recommend Anaconda or Miniconda for using the toolbox.

The toolbox works with python >=3.6

Then install the required additional packages and their dependencies:
* numpy
* pandas
* scipy
* statsmodels
* matplotlib
* configargparse
* lmfit

This can be done by typing the following into the command line (when using Anaconda or Miniconda):

```
conda install -c conda-forge numpy pandas scipy matplotlib statsmodels configargparse lmfit
```

Then the data toolbox can be installed via:

```
pip install .
```

To get an overview about the possible arguments call:
```
data_toolbox -h
```



## Authors

* **Niels Krausch**
* **Robert Giessmann**

## License

This project is licensed under the GNU General Public License v3.0.

## Citing the data-toolbox
Please acknowledge the authors of the data-toolbox if you used it for published work.
