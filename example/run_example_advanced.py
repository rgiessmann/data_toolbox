import statsmodels.formula.api as smf
import matplotlib.pyplot as plt
import numpy as np

import data_toolbox

data_toolbox.main(["-m", "example_metadata.csv"])

import pandas as pd
df = pd.read_csv("csv_files/08_conv_rates.csv")
df = df.dropna()
def return_educt_concentration_from_Condition_Name(cond_name):
    return float(cond_name.split("_dT_")[0])
df["educt_concentration"] = df.Condition_Name.apply(return_educt_concentration_from_Condition_Name)
df["mole_fraction"] = ( df.educt_concentration / 100 )

print(df)



model = smf.ols(formula='Substrate ~ mole_fraction -1', data=df).fit()
print(model.summary())



fig = plt.figure(figsize=(5,5))
ax = fig.add_subplot(111)
plt.plot([0,1],[0,1], "-", color="black", linewidth=4) # label="ideal")
plt.xlim((-0.1, 1.1))
plt.ylim((-0.1, 1.1))

plt.errorbar(x=df.mole_fraction, y=df.Substrate, yerr=df.stderr_Product, markersize=5, fmt="s", capsize=8) # label="predicted"

plt.xlabel("mole fraction (Thymidine) / []")
plt.ylabel("predicted mole fraction (Thymidine) / []")
plt.title(
    f"Predicted mole fraction of Thymidine from spectral fits \n \
    against true mole fractions. Error bars = 95%CI of fit.", fontsize=12)

ax.text(0.05, 0.95, f"R² = {round(model.rsquared,3)}", horizontalalignment="left", bbox=dict(facecolor='white', alpha=0.5))

#plt.legend()

plt.tight_layout()
plt.savefig("Graphs/output_example_advanced.svg")
plt.close("all")
